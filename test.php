﻿<?php
define('VK_API_VERSION', '5.67'); //Используемая версия API
define('VK_API_ENDPOINT', "https://api.vk.com/method/");
define('VK_API_ACCESS_TOKEN','840795c3f19dcd6251d81a3e7708d64f86b862fc8cca280825c895150f6dec93908bf3b59945202927d7b');

//Функция для вызова произвольного метода API
function _vkApi_call($method, $params = array()) {
  $params['access_token'] = VK_API_ACCESS_TOKEN;
  $params['v'] = VK_API_VERSION;
  $url = VK_API_ENDPOINT.$method.'?'.http_build_query($params);
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $json = curl_exec($curl);
  curl_close($curl);
  $response = json_decode($json, true);
  return $response['response'];
}

//Функция для вызова messages.send
function vkApi_messagesSend($peer_id, $message, $attachments = array()) {
  return _vkApi_call('messages.send', array(
    'peer_id' => $peer_id,
    'message' => $message,
    'attachment' => implode(',', $attachments)
  ));
}

vkApi_messagesSend(2000000001, 'заключительный тест');
?>
